/****** Object:  Table [dbo].[ap_order]    Script Date: 12/26/2011 11:00:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ap_order](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[date] [datetime] NOT NULL,
	[customer] [nvarchar](60) NOT NULL,
	[total] [decimal](18, 0) NOT NULL,
 CONSTRAINT [PK_ap_order] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[ap_order] ON
INSERT [dbo].[ap_order] ([id], [date], [customer], [total]) VALUES (7, CAST(0x00009FC500926490 AS DateTime), N'huan', CAST(615000 AS Decimal(18, 0)))
INSERT [dbo].[ap_order] ([id], [date], [customer], [total]) VALUES (8, CAST(0x00009FC500B3D51C AS DateTime), N'thien', CAST(355000 AS Decimal(18, 0)))
INSERT [dbo].[ap_order] ([id], [date], [customer], [total]) VALUES (9, CAST(0x00009FC500B48034 AS DateTime), N'nhan', CAST(624330000 AS Decimal(18, 0)))
SET IDENTITY_INSERT [dbo].[ap_order] OFF
/****** Object:  Table [dbo].[ap_product]    Script Date: 12/26/2011 11:00:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ap_product](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](100) NOT NULL,
	[code] [nvarchar](15) NOT NULL,
	[price] [decimal](18, 0) NOT NULL,
 CONSTRAINT [PK_ap_product] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[ap_product] ON
INSERT [dbo].[ap_product] ([id], [name], [code], [price]) VALUES (4, N'MITSUMI SCROLL', N'pvm1', CAST(95000 AS Decimal(18, 0)))
INSERT [dbo].[ap_product] ([id], [name], [code], [price]) VALUES (5, N'MITSUMI 6603 Mini', N'pvm2', CAST(130000 AS Decimal(18, 0)))
INSERT [dbo].[ap_product] ([id], [name], [code], [price]) VALUES (6, N'XScroll 120', N'pvm3', CAST(65000 AS Decimal(18, 0)))
INSERT [dbo].[ap_product] ([id], [name], [code], [price]) VALUES (7, N'Navigator 335', N'pvm4', CAST(630000 AS Decimal(18, 0)))
INSERT [dbo].[ap_product] ([id], [name], [code], [price]) VALUES (8, N'12GB DDR3 Kingmax (KIT)', N'pvr1', CAST(1650000 AS Decimal(18, 0)))
INSERT [dbo].[ap_product] ([id], [name], [code], [price]) VALUES (9, N'27" ASUS VE278Q (PIP)', N'pvlcd1', CAST(8100000 AS Decimal(18, 0)))
INSERT [dbo].[ap_product] ([id], [name], [code], [price]) VALUES (10, N'27" ASUS VK278Q (PIP)', N'pvlcd2', CAST(8600000 AS Decimal(18, 0)))
SET IDENTITY_INSERT [dbo].[ap_product] OFF
/****** Object:  Table [dbo].[ap_order_detail]    Script Date: 12/26/2011 11:00:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ap_order_detail](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[order_id] [int] NOT NULL,
	[product_id] [int] NOT NULL,
	[amount] [int] NOT NULL,
	[price] [decimal](18, 0) NOT NULL,
 CONSTRAINT [PK_ap_order_detail] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[ap_order_detail] ON
INSERT [dbo].[ap_order_detail] ([id], [order_id], [product_id], [amount], [price]) VALUES (3, 7, 4, 1, CAST(95000 AS Decimal(18, 0)))
INSERT [dbo].[ap_order_detail] ([id], [order_id], [product_id], [amount], [price]) VALUES (4, 7, 5, 4, CAST(130000 AS Decimal(18, 0)))
INSERT [dbo].[ap_order_detail] ([id], [order_id], [product_id], [amount], [price]) VALUES (5, 8, 4, 1, CAST(95000 AS Decimal(18, 0)))
INSERT [dbo].[ap_order_detail] ([id], [order_id], [product_id], [amount], [price]) VALUES (6, 8, 5, 2, CAST(130000 AS Decimal(18, 0)))
INSERT [dbo].[ap_order_detail] ([id], [order_id], [product_id], [amount], [price]) VALUES (7, 9, 7, 1, CAST(630000 AS Decimal(18, 0)))
INSERT [dbo].[ap_order_detail] ([id], [order_id], [product_id], [amount], [price]) VALUES (8, 9, 9, 77, CAST(8100000 AS Decimal(18, 0)))
SET IDENTITY_INSERT [dbo].[ap_order_detail] OFF
/****** Object:  ForeignKey [FK_ap_order_detail_ap_order]    Script Date: 12/26/2011 11:00:03 ******/
ALTER TABLE [dbo].[ap_order_detail]  WITH CHECK ADD  CONSTRAINT [FK_ap_order_detail_ap_order] FOREIGN KEY([order_id])
REFERENCES [dbo].[ap_order] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ap_order_detail] CHECK CONSTRAINT [FK_ap_order_detail_ap_order]
GO
/****** Object:  ForeignKey [FK_ap_order_detail_ap_product]    Script Date: 12/26/2011 11:00:03 ******/
ALTER TABLE [dbo].[ap_order_detail]  WITH CHECK ADD  CONSTRAINT [FK_ap_order_detail_ap_product] FOREIGN KEY([product_id])
REFERENCES [dbo].[ap_product] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ap_order_detail] CHECK CONSTRAINT [FK_ap_order_detail_ap_product]
GO
