﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace demo3Layer.libs
{
    class APCoreFormUtil
    {
        public static void clearTextBox(Panel form)
        {
            foreach (Control ctl in form.Controls)
            {
                if (ctl.GetType() == typeof(TextBox))
                {
                    TextBox txt = (TextBox)ctl;
                    txt.Clear();
                }
            }
        }
    }
}
