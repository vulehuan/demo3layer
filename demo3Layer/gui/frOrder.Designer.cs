﻿namespace demo3Layer.gui
{
    partial class frOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgvOrder = new System.Windows.Forms.DataGridView();
            this.grbFilterOrder = new System.Windows.Forms.GroupBox();
            this.txtFilterOrderCustomerKeyword = new System.Windows.Forms.TextBox();
            this.btnOrderSearch = new System.Windows.Forms.Button();
            this.radFilterOrderByCustomer = new System.Windows.Forms.RadioButton();
            this.dtpFilterOrderTo = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpFilterOrderFrom = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.radFilterOrderByDate = new System.Windows.Forms.RadioButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.form = new System.Windows.Forms.Panel();
            this.dgvOrderDetail = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panelCustomerInfo = new System.Windows.Forms.Panel();
            this.lblTotal = new System.Windows.Forms.Label();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtCustomer = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgvProduct = new System.Windows.Forms.DataGridView();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnProductSearch = new System.Windows.Forms.Button();
            this.txtProductKeyword = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.nudAmount = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.btnAddIntoBasket = new System.Windows.Forms.Button();
            this.cmsOrder = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOrder)).BeginInit();
            this.grbFilterOrder.SuspendLayout();
            this.panel2.SuspendLayout();
            this.form.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOrderDetail)).BeginInit();
            this.panelCustomerInfo.SuspendLayout();
            this.panel5.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProduct)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudAmount)).BeginInit();
            this.cmsOrder.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(830, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "QUẢN LÝ HÓA ĐƠN MUA HÀNG";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgvOrder);
            this.panel1.Controls.Add(this.grbFilterOrder);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(298, 480);
            this.panel1.TabIndex = 1;
            // 
            // dgvOrder
            // 
            this.dgvOrder.AllowUserToAddRows = false;
            this.dgvOrder.AllowUserToDeleteRows = false;
            this.dgvOrder.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvOrder.ContextMenuStrip = this.cmsOrder;
            this.dgvOrder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvOrder.Location = new System.Drawing.Point(0, 94);
            this.dgvOrder.MultiSelect = false;
            this.dgvOrder.Name = "dgvOrder";
            this.dgvOrder.ReadOnly = true;
            this.dgvOrder.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvOrder.Size = new System.Drawing.Size(298, 386);
            this.dgvOrder.TabIndex = 1;
            this.dgvOrder.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvOrder_CellClick);
            // 
            // grbFilterOrder
            // 
            this.grbFilterOrder.Controls.Add(this.txtFilterOrderCustomerKeyword);
            this.grbFilterOrder.Controls.Add(this.btnOrderSearch);
            this.grbFilterOrder.Controls.Add(this.radFilterOrderByCustomer);
            this.grbFilterOrder.Controls.Add(this.dtpFilterOrderTo);
            this.grbFilterOrder.Controls.Add(this.label3);
            this.grbFilterOrder.Controls.Add(this.dtpFilterOrderFrom);
            this.grbFilterOrder.Controls.Add(this.label2);
            this.grbFilterOrder.Controls.Add(this.radFilterOrderByDate);
            this.grbFilterOrder.Dock = System.Windows.Forms.DockStyle.Top;
            this.grbFilterOrder.Location = new System.Drawing.Point(0, 0);
            this.grbFilterOrder.Name = "grbFilterOrder";
            this.grbFilterOrder.Size = new System.Drawing.Size(298, 94);
            this.grbFilterOrder.TabIndex = 0;
            this.grbFilterOrder.TabStop = false;
            this.grbFilterOrder.Text = "Lọc theo";
            // 
            // txtFilterOrderCustomerKeyword
            // 
            this.txtFilterOrderCustomerKeyword.Location = new System.Drawing.Point(119, 65);
            this.txtFilterOrderCustomerKeyword.Name = "txtFilterOrderCustomerKeyword";
            this.txtFilterOrderCustomerKeyword.Size = new System.Drawing.Size(146, 20);
            this.txtFilterOrderCustomerKeyword.TabIndex = 5;
            this.txtFilterOrderCustomerKeyword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFilterOrderCustomerKeyword_KeyDown);
            // 
            // btnOrderSearch
            // 
            this.btnOrderSearch.Image = global::demo3Layer.Default._1324803567_search_16;
            this.btnOrderSearch.Location = new System.Drawing.Point(268, 61);
            this.btnOrderSearch.Name = "btnOrderSearch";
            this.btnOrderSearch.Size = new System.Drawing.Size(25, 25);
            this.btnOrderSearch.TabIndex = 4;
            this.btnOrderSearch.UseVisualStyleBackColor = true;
            this.btnOrderSearch.Click += new System.EventHandler(this.btnOrderSearch_Click);
            // 
            // radFilterOrderByCustomer
            // 
            this.radFilterOrderByCustomer.AutoSize = true;
            this.radFilterOrderByCustomer.Location = new System.Drawing.Point(9, 65);
            this.radFilterOrderByCustomer.Name = "radFilterOrderByCustomer";
            this.radFilterOrderByCustomer.Size = new System.Drawing.Size(104, 17);
            this.radFilterOrderByCustomer.TabIndex = 3;
            this.radFilterOrderByCustomer.Text = "Tên khách hàng";
            this.radFilterOrderByCustomer.UseVisualStyleBackColor = true;
            // 
            // dtpFilterOrderTo
            // 
            this.dtpFilterOrderTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFilterOrderTo.Location = new System.Drawing.Point(176, 39);
            this.dtpFilterOrderTo.Name = "dtpFilterOrderTo";
            this.dtpFilterOrderTo.Size = new System.Drawing.Size(86, 20);
            this.dtpFilterOrderTo.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(150, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Đến";
            // 
            // dtpFilterOrderFrom
            // 
            this.dtpFilterOrderFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFilterOrderFrom.Location = new System.Drawing.Point(53, 39);
            this.dtpFilterOrderFrom.Name = "dtpFilterOrderFrom";
            this.dtpFilterOrderFrom.Size = new System.Drawing.Size(86, 20);
            this.dtpFilterOrderFrom.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Từ";
            // 
            // radFilterOrderByDate
            // 
            this.radFilterOrderByDate.AutoSize = true;
            this.radFilterOrderByDate.Checked = true;
            this.radFilterOrderByDate.Location = new System.Drawing.Point(9, 19);
            this.radFilterOrderByDate.Name = "radFilterOrderByDate";
            this.radFilterOrderByDate.Size = new System.Drawing.Size(50, 17);
            this.radFilterOrderByDate.TabIndex = 0;
            this.radFilterOrderByDate.TabStop = true;
            this.radFilterOrderByDate.Text = "Ngày";
            this.radFilterOrderByDate.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.form);
            this.panel2.Controls.Add(this.panel5);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(298, 24);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(532, 480);
            this.panel2.TabIndex = 2;
            // 
            // form
            // 
            this.form.Controls.Add(this.dgvOrderDetail);
            this.form.Controls.Add(this.panelCustomerInfo);
            this.form.Dock = System.Windows.Forms.DockStyle.Fill;
            this.form.Location = new System.Drawing.Point(0, 201);
            this.form.Name = "form";
            this.form.Size = new System.Drawing.Size(532, 255);
            this.form.TabIndex = 5;
            // 
            // dgvOrderDetail
            // 
            this.dgvOrderDetail.AllowUserToAddRows = false;
            this.dgvOrderDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvOrderDetail.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.name,
            this.amount,
            this.price});
            this.dgvOrderDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvOrderDetail.Location = new System.Drawing.Point(0, 85);
            this.dgvOrderDetail.MultiSelect = false;
            this.dgvOrderDetail.Name = "dgvOrderDetail";
            this.dgvOrderDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvOrderDetail.Size = new System.Drawing.Size(532, 170);
            this.dgvOrderDetail.TabIndex = 13;
            this.dgvOrderDetail.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dgvOrderDetail_CellBeginEdit);
            this.dgvOrderDetail.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvOrderDetail_CellEndEdit);
            // 
            // id
            // 
            this.id.DataPropertyName = "id";
            this.id.HeaderText = "ID";
            this.id.Name = "id";
            this.id.Visible = false;
            // 
            // name
            // 
            this.name.DataPropertyName = "name";
            this.name.HeaderText = "Tên sản phẩm";
            this.name.Name = "name";
            this.name.ReadOnly = true;
            this.name.Width = 240;
            // 
            // amount
            // 
            this.amount.DataPropertyName = "amount";
            this.amount.HeaderText = "Số lượng";
            this.amount.Name = "amount";
            // 
            // price
            // 
            this.price.DataPropertyName = "price";
            this.price.HeaderText = "Đơn giá";
            this.price.Name = "price";
            // 
            // panelCustomerInfo
            // 
            this.panelCustomerInfo.Controls.Add(this.lblTotal);
            this.panelCustomerInfo.Controls.Add(this.dtpDate);
            this.panelCustomerInfo.Controls.Add(this.label8);
            this.panelCustomerInfo.Controls.Add(this.label7);
            this.panelCustomerInfo.Controls.Add(this.txtCustomer);
            this.panelCustomerInfo.Controls.Add(this.label6);
            this.panelCustomerInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCustomerInfo.Location = new System.Drawing.Point(0, 0);
            this.panelCustomerInfo.Name = "panelCustomerInfo";
            this.panelCustomerInfo.Size = new System.Drawing.Size(532, 85);
            this.panelCustomerInfo.TabIndex = 12;
            // 
            // lblTotal
            // 
            this.lblTotal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTotal.Location = new System.Drawing.Point(115, 56);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(204, 19);
            this.lblTotal.TabIndex = 16;
            this.lblTotal.Tag = "0";
            this.lblTotal.Text = "0";
            this.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dtpDate
            // 
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDate.Location = new System.Drawing.Point(115, 30);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(204, 20);
            this.dtpDate.TabIndex = 15;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 58);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "Tổng tiền";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 32);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "Ngày lập";
            // 
            // txtCustomer
            // 
            this.txtCustomer.Location = new System.Drawing.Point(115, 5);
            this.txtCustomer.Name = "txtCustomer";
            this.txtCustomer.Size = new System.Drawing.Size(204, 20);
            this.txtCustomer.TabIndex = 14;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 6);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(86, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Tên khách hàng";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.btnAdd);
            this.panel5.Controls.Add(this.btnSave);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel5.Location = new System.Drawing.Point(0, 456);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(532, 24);
            this.panel5.TabIndex = 4;
            // 
            // btnAdd
            // 
            this.btnAdd.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnAdd.Image = global::demo3Layer.Default._1324821687_Add;
            this.btnAdd.Location = new System.Drawing.Point(485, 0);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(25, 24);
            this.btnAdd.TabIndex = 7;
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Visible = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnSave
            // 
            this.btnSave.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnSave.Image = global::demo3Layer.Default._1324804068_disk;
            this.btnSave.Location = new System.Drawing.Point(510, 0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(22, 24);
            this.btnSave.TabIndex = 1;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(0, 177);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(532, 24);
            this.label5.TabIndex = 1;
            this.label5.Text = "THÔNG TIN HÓA ĐƠN";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dgvProduct);
            this.groupBox2.Controls.Add(this.panel4);
            this.groupBox2.Controls.Add(this.panel3);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(532, 177);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Danh sách sản phẩm";
            // 
            // dgvProduct
            // 
            this.dgvProduct.AllowUserToAddRows = false;
            this.dgvProduct.AllowUserToDeleteRows = false;
            this.dgvProduct.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProduct.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvProduct.Location = new System.Drawing.Point(3, 45);
            this.dgvProduct.MultiSelect = false;
            this.dgvProduct.Name = "dgvProduct";
            this.dgvProduct.ReadOnly = true;
            this.dgvProduct.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvProduct.Size = new System.Drawing.Size(526, 105);
            this.dgvProduct.TabIndex = 2;
            this.dgvProduct.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvProduct_CellClick);
            this.dgvProduct.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvProduct_CellContentClick);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnProductSearch);
            this.panel4.Controls.Add(this.txtProductKeyword);
            this.panel4.Controls.Add(this.label4);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(3, 16);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(526, 29);
            this.panel4.TabIndex = 1;
            // 
            // btnProductSearch
            // 
            this.btnProductSearch.Image = global::demo3Layer.Default._1324803567_search_16;
            this.btnProductSearch.Location = new System.Drawing.Point(417, 2);
            this.btnProductSearch.Name = "btnProductSearch";
            this.btnProductSearch.Size = new System.Drawing.Size(21, 23);
            this.btnProductSearch.TabIndex = 6;
            this.btnProductSearch.UseVisualStyleBackColor = true;
            this.btnProductSearch.Click += new System.EventHandler(this.btnProductSearch_Click);
            // 
            // txtProductKeyword
            // 
            this.txtProductKeyword.Location = new System.Drawing.Point(162, 4);
            this.txtProductKeyword.Name = "txtProductKeyword";
            this.txtProductKeyword.Size = new System.Drawing.Size(251, 20);
            this.txtProductKeyword.TabIndex = 1;
            this.txtProductKeyword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtProductKeyword_KeyDown);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 7);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(144, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Nhập tên hoặc mã sản phẩm";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel7);
            this.panel3.Controls.Add(this.btnAddIntoBasket);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(3, 150);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(526, 24);
            this.panel3.TabIndex = 0;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.txtPrice);
            this.panel7.Controls.Add(this.label10);
            this.panel7.Controls.Add(this.nudAmount);
            this.panel7.Controls.Add(this.label9);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel7.Location = new System.Drawing.Point(183, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(317, 24);
            this.panel7.TabIndex = 1;
            // 
            // txtPrice
            // 
            this.txtPrice.Location = new System.Drawing.Point(184, 2);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(127, 20);
            this.txtPrice.TabIndex = 3;
            this.txtPrice.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(134, 6);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "Giá bán";
            // 
            // nudAmount
            // 
            this.nudAmount.Location = new System.Drawing.Point(56, 2);
            this.nudAmount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudAmount.Name = "nudAmount";
            this.nudAmount.Size = new System.Drawing.Size(63, 20);
            this.nudAmount.TabIndex = 1;
            this.nudAmount.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(4, 6);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Số lượng";
            // 
            // btnAddIntoBasket
            // 
            this.btnAddIntoBasket.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnAddIntoBasket.Image = global::demo3Layer.Default._1324803705_download_manager;
            this.btnAddIntoBasket.Location = new System.Drawing.Point(500, 0);
            this.btnAddIntoBasket.Name = "btnAddIntoBasket";
            this.btnAddIntoBasket.Size = new System.Drawing.Size(26, 24);
            this.btnAddIntoBasket.TabIndex = 0;
            this.btnAddIntoBasket.UseVisualStyleBackColor = true;
            this.btnAddIntoBasket.Click += new System.EventHandler(this.btnAddIntoBasket_Click);
            // 
            // cmsOrder
            // 
            this.cmsOrder.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteToolStripMenuItem});
            this.cmsOrder.Name = "cmsOrder";
            this.cmsOrder.Size = new System.Drawing.Size(153, 48);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.deleteToolStripMenuItem.Text = "Xóa";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // frOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(830, 504);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Name = "frOrder";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "QUAN LY HOA DON MUA HANG";
            this.Load += new System.EventHandler(this.frOrder_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvOrder)).EndInit();
            this.grbFilterOrder.ResumeLayout(false);
            this.grbFilterOrder.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.form.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvOrderDetail)).EndInit();
            this.panelCustomerInfo.ResumeLayout(false);
            this.panelCustomerInfo.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvProduct)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudAmount)).EndInit();
            this.cmsOrder.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dgvOrder;
        private System.Windows.Forms.GroupBox grbFilterOrder;
        private System.Windows.Forms.Button btnOrderSearch;
        private System.Windows.Forms.RadioButton radFilterOrderByCustomer;
        private System.Windows.Forms.DateTimePicker dtpFilterOrderTo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtpFilterOrderFrom;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton radFilterOrderByDate;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnAddIntoBasket;
        private System.Windows.Forms.TextBox txtFilterOrderCustomerKeyword;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView dgvProduct;
        private System.Windows.Forms.Button btnProductSearch;
        private System.Windows.Forms.TextBox txtProductKeyword;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Panel form;
        private System.Windows.Forms.Panel panelCustomerInfo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtCustomer;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView dgvOrderDetail;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TextBox txtPrice;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown nudAmount;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn name;
        private System.Windows.Forms.DataGridViewTextBoxColumn amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn price;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.ContextMenuStrip cmsOrder;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
    }
}