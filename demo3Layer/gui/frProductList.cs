﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using demo3Layer.dto;
using demo3Layer.bll;

namespace demo3Layer.gui
{
    public partial class frProductList : Form
    {
        public frProductList()
        {
            InitializeComponent();
        }
        /*
         * actin when click btnAdd
         * */
        private void btnAdd_Click(object sender, EventArgs e)
        {
            frProduct f = new frProduct();
            f.ShowDialog();
            int positionSelected = dgvProduct.SelectedRows.Count > 0 ? dgvProduct.SelectedRows[0].Index : 0;

            loadGridView();
            dgvProduct.Rows[positionSelected].Selected = true;
        }
        /*
         * set the dgvProduct data source
         * */
        void loadGridView(List<Product> list)
        {
            dgvProduct.DataSource = null;
            dgvProduct.DataSource = list;
            dgvProduct.Columns["id"].Visible = false;
            dgvProduct.Columns["name"].Width = 240;
        }
        void loadGridView()
        {
            List<Product> list = ProductBll.getList();
            loadGridView(list);
        }
        /*
         * 
         * */
        private void frProductList_Load(object sender, EventArgs e)
        {
            loadGridView();
        }
        /*
         * action when update an item
         * */
        private void updateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgvProduct.SelectedRows.Count == 0)
            {
                return;
            }

            frProduct f = new frProduct();
            f.obj = (Product) dgvProduct.SelectedRows[0].DataBoundItem;
            f.ShowDialog();

            int positionSelected = dgvProduct.SelectedRows.Count > 0 ? dgvProduct.SelectedRows[0].Index : 0;

            loadGridView();
            dgvProduct.Rows[positionSelected].Selected = true;
        }
        /*
         * action when delete an item
         * */
        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgvProduct.SelectedRows.Count == 0)
            {
                return;
            }
            if (MessageBox.Show(Default.DeleteConfirm, Default.DeleteConfirmTitle, MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {
                Product obj = (Product)dgvProduct.SelectedRows[0].DataBoundItem;
                if (new ProductBll().delete(obj))
                {
                    MessageBox.Show(Default.DeleteSuccessfully);

                    List<Product> list = (List<Product>)dgvProduct.DataSource;
                    list.Remove(obj);
                    
                    int positionSelected = dgvProduct.SelectedRows.Count > 0 ? dgvProduct.SelectedRows[0].Index : 0;
                    loadGridView(list);
                    if (dgvProduct.Rows.Count > 0)
                    {
                        if (positionSelected != 0)
                        {
                            positionSelected--;
                        }
                        
                        dgvProduct.Rows[positionSelected].Selected = true;
                    }
                }
                else
                {
                    MessageBox.Show(Default.DeleteFailed);
                }
            }
        }

        private void dgvProduct_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {

        }
        /*
         * action when search
         * */
        private void btnSearch_Click(object sender, EventArgs e)
        {
            List<Product> list = ProductBll.getList(txtKeyword.Text);
            loadGridView(list);
        }
    }
}
