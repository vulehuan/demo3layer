﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using demo3Layer.dto;
using demo3Layer.bll;
using demo3Layer.libs;

namespace demo3Layer.gui
{
    public partial class frProduct : Form
    {
        public Product obj = null;
        
        public frProduct()
        {
            InitializeComponent();
        }

        private void frProduct_Load(object sender, EventArgs e)
        {
            //if update
            if (obj != null)
            {
                //---display information
                txtName.Text = obj.Name;
                txtCode.Text = obj.Code;
                txtPrice.Text = obj.Price.ToString();
            }
        }
        /*
         * filter product input
         * */
        private Product doInput(Product obj)
        {
            if (obj == null)
            {
                obj = new Product();
            }
            obj.Name = txtName.Text;
            obj.Code = txtCode.Text;
            obj.Price = Convert.ToDecimal(txtPrice.Text);
            return obj;
        }
        /*
         * action when save
         * */
        private void btnSave_Click(object sender, EventArgs e)
        {
            //validate form
            string error = ProductBll.checkForm(form, obj);
            //if error
            if (error != "")
            {
                MessageBox.Show(error);
                return;
            }            

            if (obj == null)//add new
            {
                obj = doInput(obj);
                if (new ProductBll().insert(obj))
                {
                    obj = null;
                    MessageBox.Show(Default.AddNewSuccessfully);
                    APCoreFormUtil.clearTextBox(form);

                    txtName.Focus();
                }
                else
                {
                    MessageBox.Show(Default.AddNewFailed);
                }
            }
            else//update
            {
                obj = doInput(obj);

                if (new ProductBll().update(obj))
                {
                    MessageBox.Show(Default.UpdateSuccessfully);
                    this.Close();
                }
                else
                {
                    MessageBox.Show(Default.UpdateFailed);
                }
            }
        }
    }
}
