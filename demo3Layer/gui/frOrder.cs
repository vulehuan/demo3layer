﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using demo3Layer.dto;
using demo3Layer.bll;

namespace demo3Layer.gui
{
    public partial class frOrder : Form
    {
        public frOrder()
        {
            InitializeComponent();
        }

        private void frOrder_Load(object sender, EventArgs e)
        {
            List<Product> productList = ProductBll.getList();
            loadGridView(productList);

            dtpFilterOrderFrom.Value = DateTime.Now.AddDays(-7);
            dtpFilterOrderTo.Value = DateTime.Now.AddDays(1);

            doFilterOrder();
        }
        /*
        * set the datasource & format dgvProduct datagridview
        * */
        void loadGridView(List<Product> list)
        {
            dgvProduct.DataSource = null;
            dgvProduct.DataSource = list;
            dgvProduct.Columns["id"].Visible = false;
            dgvProduct.Columns["name"].Width = 240;

            dgvProduct.Columns["name"].HeaderText = "Tên sản phẩm";
            dgvProduct.Columns["code"].HeaderText = "Mã sản phẩm";
            dgvProduct.Columns["price"].HeaderText = "Giá gốc";

            clickProductRow();
        }
        void loadGridView()
        {
            List<Product> list = ProductBll.getList();
            loadGridView(list);
        }
        /*
         * product search function
         * */
        void doProductSearch()
        {
            List<Product> list = ProductBll.getList(txtProductKeyword.Text);
            loadGridView(list);
        }
        /*
         * action when user click btnProductSearch
         * */
        private void btnProductSearch_Click(object sender, EventArgs e)
        {
            doProductSearch();
        }
        /*
         * action when user enter on txtProductKeyword
         * */
        private void txtProductKeyword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                doProductSearch();
            }
        }

        /*
         * action when user add product to basket
         * */
        private void btnAddIntoBasket_Click(object sender, EventArgs e)
        {
            if (dgvProduct.SelectedRows.Count > 0)
            {
                Product obj = (Product) dgvProduct.SelectedRows[0].DataBoundItem;
                bool notFound = true;

                foreach (DataGridViewRow dgvr in dgvOrderDetail.Rows)
                {
                    //found product in basket
                    if (dgvr.Cells["id"].Value.ToString() == obj.Id.ToString())
                    {
                        //---select it
                        dgvr.Selected = true;
                        notFound = false;
                        break;
                    }
                }
                
                if (!notFound)
                {
                    MessageBox.Show(String.Format(Error.DuplicateOrderProductId, obj.Code));                    
                }
                else{
                    //not found --> add new product to basket
                    DataTable dt = (DataTable)dgvOrderDetail.DataSource;
                    if (dt == null)
                    {
                        dt = new DataTable();
                        dt.Columns.Add("id");
                        dt.Columns.Add("name");
                        dt.Columns.Add("amount");
                        dt.Columns.Add("price");
                    }

                    DataRow dr = dt.NewRow();
                    dr["id"] = obj.Id;
                    dr["name"] = obj.Name;
                    dr["amount"] = nudAmount.Value;
                    decimal price = 0;
                    try
                    {
                        price = decimal.Parse(txtPrice.Text);
                    }
                    catch (Exception)
                    {
                    }
                    dr["price"] = price;

                    dt.Rows.Add(dr);
                    dgvOrderDetail.DataSource = dt;
                    formatOrderDetailGridView();

                    //
                    decimal total = nudAmount.Value * price + decimal.Parse(lblTotal.Tag.ToString());
                    formatTotal(total);
                }
            }
        }
        /*
         * format text for lblTotal
         * */
        void formatTotal(decimal total)
        {
            lblTotal.Tag = total;
            lblTotal.Text = String.Format("{0:C}", total);
        }
        /*
         * action when user choose a row
         * */
        void clickProductRow()
        {
            if (dgvProduct.SelectedRows.Count > 0)
            {
                Product obj = (Product)dgvProduct.SelectedRows[0].DataBoundItem;
                txtPrice.Text = obj.Price.ToString();
            }
        }
        /*
         * when user choose a row
         * */
        private void dgvProduct_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            clickProductRow();
        }
        /*
         * After edit, re-calculate total pay
         * */
        private void dgvOrderDetail_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvOrderDetail.SelectedRows.Count > 0)
            {
                //get new information
                DataGridViewRow dgvr = dgvOrderDetail.SelectedRows[0];
                int newAmount;
                try
                {
                    newAmount = int.Parse(dgvr.Cells["amount"].Value.ToString());
                }
                catch (Exception)
                {
                    newAmount = 1;
                    dgvr.Cells["amount"].Value = 0;
                }
                decimal newPrice;
                try
                {
                    newPrice = decimal.Parse(dgvr.Cells["price"].Value.ToString());
                }
                catch (Exception)//if user enter none numeric character
                {
                    newPrice = 0;
                    dgvr.Cells["price"].Value = 0;
                }
                decimal newSubTotal = newAmount * newPrice;
                //if have error --> set to default
                if (newAmount <= 0)
                {
                    MessageBox.Show(Error.InvalidProductAmount);
                    dgvr.Cells["amount"].Value = 1;
                }
                if (newPrice < 0)
                {
                    MessageBox.Show(Error.InvalidProductPrice);
                    dgvr.Cells["price"].Value = 0;
                }
                //get old information
                int oldAmount = (int)nudAmount.Tag;
                decimal oldPrice = (decimal)txtPrice.Tag;
                decimal oldSubTotal = oldAmount * oldPrice;
                //re-calculate total
                decimal total = (decimal)lblTotal.Tag;
                total = total - oldSubTotal + newSubTotal;
                formatTotal(total);
            }
        }
        /*
         * Before edit, save current information into anywhere you like
         * */
        private void dgvOrderDetail_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (dgvOrderDetail.SelectedRows.Count > 0)
            {
                DataGridViewRow dgvr = dgvOrderDetail.SelectedRows[0];
                nudAmount.Tag = int.Parse(dgvr.Cells["amount"].Value.ToString());
                txtPrice.Tag = decimal.Parse(dgvr.Cells["price"].Value.ToString());
            }
            
        }
        void resetForAddNew()
        {
            txtCustomer.Text = "";
            lblTotal.Tag = 0;
            lblTotal.Text = "0";

            dgvOrderDetail.DataSource=null;
        }
        /*
         * action when add new order
         * */
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (dgvOrderDetail.Rows.Count == 0)
            {
                MessageBox.Show(Error.InvalidBasketItemCount);
                return;
            }

            string error = OrderBll.checkForm(form);
            if (error != "")
            {
                MessageBox.Show(error);
                return;
            }
            //order detail information
            List<OrderDetail> list = new List<OrderDetail>();
            foreach (DataGridViewRow dgvr in dgvOrderDetail.Rows)
            {
                OrderDetail item = new OrderDetail();
                item.Product_id = int.Parse(dgvr.Cells["id"].Value.ToString());
                item.Amount = int.Parse(dgvr.Cells["amount"].Value.ToString());
                item.Price = decimal.Parse(dgvr.Cells["price"].Value.ToString());

                list.Add(item);
            }
            //order information
            Order obj = new Order();
            obj.Customer = txtCustomer.Text;
            obj.Date = dtpDate.Value;
            obj.Total = (decimal) lblTotal.Tag;
            obj.Ap_order_detail = list;
            //insert
            if (new OrderBll().insert(obj))
            {
                MessageBox.Show(Default.AddNewSuccessfully);
                resetForAddNew();

                doFilterOrder();
            }
            else
            {
                MessageBox.Show(Default.AddNewFailed);
            }
        }
        /*
         * 
         * */
        void loadOrderGridView(List<Order> list)
        {
            dgvOrder.DataSource = null;
            dgvOrder.DataSource = list;
            dgvOrder.Columns["id"].Visible = false;
        }
        void doFilterOrder()
        {
            List<Order> list = OrderBll.getList(grbFilterOrder);
            loadOrderGridView(list);
        }
        /*
         * 
         * */
        private void btnOrderSearch_Click(object sender, EventArgs e)
        {
            doFilterOrder();
        }

        private void txtFilterOrderCustomerKeyword_KeyDown(object sender, KeyEventArgs e)
        {
            radFilterOrderByCustomer.Checked = true;
            if (e.KeyCode == Keys.Enter)
            {
                doFilterOrder();
            }
        }
        void displayAddNew(bool flag)
        {
            btnAdd.Visible = flag;
            if (dgvOrderDetail.Rows.Count > 0)
            {
                dgvOrderDetail.Columns["amount"].ReadOnly = flag;
                dgvOrderDetail.Columns["price"].ReadOnly = flag;
            }
            txtCustomer.ReadOnly = flag;
            dtpDate.Enabled = !flag;
            btnAddIntoBasket.Visible = !flag;
            btnSave.Visible = !flag;
        }
        private void dgvOrder_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvOrder.SelectedRows.Count > 0)
            {
                Order obj = (Order)dgvOrder.SelectedRows[0].DataBoundItem;
                txtCustomer.Text = obj.Customer;
                dtpDate.Value = obj.Date;
                formatTotal(obj.Total);

                List<OrderDetail> list = OrderBll.Ap_order_detail(obj);
                DataTable dt = new DataTable();
                dt.Columns.Add("id");
                dt.Columns.Add("name");
                dt.Columns.Add("amount");
                dt.Columns.Add("price");
                foreach (OrderDetail item in list)
                {
                    DataRow dr = dt.NewRow();
                    dr["id"] = item.Product_id;
                    dr["name"] = item.Product_name;
                    dr["amount"] = item.Amount;
                    dr["price"] = item.Price;
                    dt.Rows.Add(dr);
                }
                dgvOrderDetail.DataSource = dt;

                formatOrderDetailGridView();
            }
            displayAddNew(true);
        }

        void formatOrderDetailGridView()
        {
            if (dgvOrderDetail.Rows.Count > 0)
            {
                dgvOrderDetail.Columns["id"].Visible = false;
                dgvOrderDetail.Columns["name"].HeaderText = "Tên sản phẩm";
                dgvOrderDetail.Columns["amount"].HeaderText = "Số lượng";
                dgvOrderDetail.Columns["price"].HeaderText = "Giá bán";
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            resetForAddNew();
            displayAddNew(false);
        }
        /*
         * 
         * */
        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dgvOrder.SelectedRows.Count == 0)
            {
                return;
            }
            if (MessageBox.Show(Default.DeleteConfirm, Default.DeleteConfirmTitle, MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {
                Order obj = (Order)dgvOrder.SelectedRows[0].DataBoundItem;
                if (new OrderBll().delete(obj))
                {
                    MessageBox.Show(Default.DeleteSuccessfully);

                    List<Order> list = (List<Order>)dgvOrder.DataSource;
                    list.Remove(obj);

                    int positionSelected = dgvOrder.SelectedRows.Count > 0 ? dgvOrder.SelectedRows[0].Index : 0;
                    loadOrderGridView(list);
                    if (dgvOrder.Rows.Count > 0)
                    {
                        if (positionSelected != 0)
                        {
                            positionSelected--;
                        }

                        dgvOrder.Rows[positionSelected].Selected = true;

                    }
                }
                else
                {
                    MessageBox.Show(Default.DeleteFailed);
                }
            }
        }

        private void dgvProduct_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
