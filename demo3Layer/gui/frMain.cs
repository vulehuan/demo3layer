﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace demo3Layer.gui
{
    public partial class frMain : Form
    {
        public frMain()
        {
            InitializeComponent();
        }
        /*
         * 
         * */
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        /*
         * 
         * */
        void openFormOnlyOnce(Form f)
        {
            foreach (Form childForm in this.MdiChildren)
            {
                if (childForm.GetType() == f.GetType())
                {
                    childForm.Focus();
                    return;
                }
            }
            f.MdiParent = this;
            f.Show();
        }
        /*
         * 
         * */
        private void productListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frProductList f = new frProductList();
            openFormOnlyOnce(f);
        }
        /*
         * 
         * */
        private void orderListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frOrder f = new frOrder();
            openFormOnlyOnce(f);
        }
    }
}
