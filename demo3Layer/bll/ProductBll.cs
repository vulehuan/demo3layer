﻿using System;
using demo3Layer.dto;
using demo3Layer.dal;
using System.Windows.Forms;
using System.Collections.Generic;

namespace demo3Layer.bll
{
	public class ProductBll
	{
		public ProductBll()
		{
		}
        /*
         * @todo validate form
         * 
         * */
        public static string checkForm(Panel form, Product obj)
        {
            string result = "";
            TextBox txtName = (TextBox)form.Controls["txtName"];
            TextBox txtCode = (TextBox)form.Controls["txtCode"];
            TextBox txtPrice = (TextBox)form.Controls["txtPrice"];
            if (txtName.Text.Trim().Length == 0)
            {
                result += Error.BlankProductName + "\r\n";
            }
            if (txtCode.Text.Trim().Length == 0)
            {
                result += Error.BlankProductCode + "\r\n";
            }
            if (txtPrice.Text.Trim().Length == 0)
            {
                result += Error.BlankProductPrice + "\r\n";
            }
            else
            {
                try
                {
                    decimal test = Convert.ToDecimal(txtPrice.Text);
                }
                catch (Exception)
                {
                    result += Error.InvalidProductPrice + "\r\n";
                }
            }
            if (result == "")
            {
                if (obj == null)
                {
                    obj = new Product();
                }
                obj.Code = txtCode.Text;
                if (isProductCodeExist(obj))
                {
                    result += Error.DuplicateProductCode + "\r\n";
                }
            }
            return result;
        }
        private static bool isProductCodeExist(Product currentObj)
        {
            return new ProductDal().getObjectByProductCode(currentObj) != null;
        }
        /*
         * @todo get product list
         * */
        public static List<Product> getList()
        {
            return ProductDal.getList();
        }
        public static List<Product> getList(string keyword)
        {
            return ProductDal.getList(keyword);
        }
        /*
         * @todo add new object
         * */
        public bool insert(Product obj)
        {
            return new ProductDal().insert(obj);
        }
        /*
         * @todo update object
         * */
        public bool update(Product obj)
        {
            return new ProductDal().update(obj);
        }
        /*
        * @todo delete object
        * */
        public bool delete(Product obj)
        {
            return new ProductDal().delete(obj);
        }
	}
}
