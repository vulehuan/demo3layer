﻿using System;
using demo3Layer.dto;
using System.Windows.Forms;
using demo3Layer.dal;
using System.Collections.Generic;

namespace demo3Layer.bll
{
	public class OrderBll
	{
        private Order obj;
		public OrderBll()
		{
		}
        /*
         * @todo validate form
         * 
         * */
        public static string checkForm(Panel form)
        {
            string result = "";
            Panel panelCustomerInfo = (Panel)form.Controls["panelCustomerInfo"];
            TextBox txtCustomer = (TextBox)panelCustomerInfo.Controls["txtCustomer"];
            if (txtCustomer.Text.Trim().Length == 0)
            {
                result += Error.BlankCustomerName + "\r\n";
            }
           
            return result;
        }
        /*
         * @todo filter order
         * */
        public static List<Order> getList(GroupBox grbFilterOrder)
        {
            List<Order> result = new List<Order>();
            RadioButton radFilterOrderByDate = (RadioButton)grbFilterOrder.Controls["radFilterOrderByDate"];

            //search by date
            if (radFilterOrderByDate.Checked)
            {
                DateTimePicker dtpFilterOrderFrom = (DateTimePicker)grbFilterOrder.Controls["dtpFilterOrderFrom"];
                DateTimePicker dtpFilterOrderTo = (DateTimePicker)grbFilterOrder.Controls["dtpFilterOrderTo"];
                result = OrderDal.getListByDate(dtpFilterOrderFrom.Value, dtpFilterOrderTo.Value);
            }
            else//search by customer name
            {
                TextBox txtFilterOrderCustomerKeyword = (TextBox)grbFilterOrder.Controls["txtFilterOrderCustomerKeyword"];
                result = OrderDal.getListByCustomer(txtFilterOrderCustomerKeyword.Text);
            }
            return result;
        }
        public static List<OrderDetail> Ap_order_detail(Order obj)
        {
            return OrderDal.Ap_order_detail(obj);
        }
        /*
        * @todo add new object
        * */
        public bool insert(Order obj)
        {
            return new OrderDal().insert(obj);
        }
        /*
        * @todo delete object
        * */
        public bool delete(Order obj)
        {
            new OrderDal().deleteOrderDetail(obj);
            bool result = new OrderDal().delete(obj);
            return result;
        }
	}
}
