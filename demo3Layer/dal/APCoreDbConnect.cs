﻿using System;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Data;

namespace demo3Layer.dal
{
	public class APCoreDbConnect
	{
        /*
        * @todo read from database
        * */
        protected static DataTable read(string sql)
        {
            DataTable result = new DataTable();
            if (!sql.ToUpper().Contains("SELECT"))
            {
                sql = "SELECT * FROM " + sql;
            }
            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter(sql, demo3Layer.Properties.Settings.Default.apConnectionString);
                adapter.FillSchema(result, SchemaType.Source);
                adapter.Fill(result);
            }
            catch (Exception ex)
            {
                MessageBox.Show(Error.AnErrorHasBeenOccured + "\r\n" + ex.Message);
            }
            return result;
        }
        /*
         * @todo read database table 's structure
         * */
        protected static DataTable readStructure(string tableName)
        {
            DataTable result = new DataTable();
            string sql = "SELECT * FROM " + tableName;

            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter(sql, demo3Layer.Properties.Settings.Default.apConnectionString);
                adapter.FillSchema(result, SchemaType.Source);
            }
            catch (Exception ex)
            {
                MessageBox.Show(Error.AnErrorHasBeenOccured + "\r\n" + ex.Message);
            }
            return result;
        }
        /*
         * @todo write to database
         * */
        protected static int write(DataRow dr)
        {
            int result = 0;
            string sql = "SELECT * FROM " + dr.Table.TableName;
            DataTable dt = dr.Table;
            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter(sql, demo3Layer.Properties.Settings.Default.apConnectionString);
                SqlCommandBuilder cmdBuilder = new SqlCommandBuilder(adapter);
                result = adapter.Update(dt);
            }
            catch (Exception ex)
            {
                MessageBox.Show(Error.AnErrorHasBeenOccured + "\r\n" + ex.Message);
            }
            return result;
        }
        /*
         * @todo: excute a sql
         * if insert, return indentity id
         * */
        protected static int executeNonQuery(string sql)
        {
            int count = 0;
            using (SqlConnection conn = new SqlConnection(demo3Layer.Properties.Settings.Default.apConnectionString))
            {
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }
                SqlCommand cmd = new SqlCommand(sql, conn);
                try
                {
                    count = cmd.ExecuteNonQuery();

                    if (sql.ToUpper().IndexOf("INSERT INTO") == 0)
                    {
                        sql="Select @@Identity";
                        cmd.CommandText = sql;
                        count = int.Parse(cmd.ExecuteScalar().ToString());
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(Error.AnErrorHasBeenOccured + "\r\n" + ex.Message);
                }
            }
            return count;
        }
        /*
         * @todo: get a single value
         * */
        protected static object executeScalar(string sql)
        {
            object result = null;
            using (SqlConnection conn = new SqlConnection(demo3Layer.Properties.Settings.Default.apConnectionString))
            {
                if (conn.State == ConnectionState.Closed)
                {
                    conn.Open();
                }
                SqlCommand cmd = new SqlCommand(sql, conn);
                try
                {
                    result = cmd.ExecuteScalar();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(Error.AnErrorHasBeenOccured + "\r\n" + ex.Message);
                }
            }
            return result;
        }
	}
}
