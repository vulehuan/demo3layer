﻿using System;
using System.Collections.Generic;
using System.Text;
using demo3Layer.dto;
using System.Data;
using demo3Layer.libs;
using System.Windows.Forms;

namespace demo3Layer.dal
{
    class OrderDal:APCoreDbConnect
    {
        public OrderDal()
        {
        
        }
        /*
         * @todo filter order
         * */
        private static List<Order> getList(string sql)
        {
            DataTable dt = read(sql);
            List<Order> list = new List<Order>();
            foreach (DataRow dr in dt.Rows)
            {
                list.Add(new Order(dr));
            }
            return list;
        }
        public static List<Order> getListByDate(DateTime dt1, DateTime dt2)
        {
             string sql = "SELECT * FROM [ap_order] WHERE [date] Between '{0}-{1}-{2}' And '{3}-{4}-{5}' ORDER BY [date] DESC";
             sql = String.Format(sql, dt1.Year, dt1.Month, dt1.Day, dt2.Year, dt2.Month, dt2.Day);
             return getList(sql);
        }
        public static List<Order> getListByCustomer(string customer)
        {
            string sql = "SELECT * FROM [ap_order] WHERE [customer] like '%{0}%' ORDER BY [date] DESC";
            sql = String.Format(sql, APCoreFilter.clean(customer));
            return getList(sql);
        }

        /*
         * @todo insert new object to database
         * */
        public bool insert(Order obj)
        {
            string sql = "INSERT INTO [ap_order]([date],[customer],[total])VALUES('" + DateTime.Now + "',N'" + APCoreFilter.clean(obj.Customer) + "'," + obj.Total + ")";
            int orderId = int.Parse(executeNonQuery(sql).ToString());

            List<OrderDetail> list = obj.Ap_order_detail;

            sql = "";
            foreach (OrderDetail item in list)
            {
                sql += "INSERT INTO [ap_order_detail] ([order_id],[product_id],[amount],[price])VALUES"
                        +"("+orderId+","+item.Product_id+","+item.Amount+","+item.Price+");";
            }
            executeNonQuery(sql);

            return orderId > 0;
        }
        /*
        * @todo delete object from database
        * */
        public bool delete(Order obj)
        {
            string sql = "DELETE FROM ap_order WHERE id=" + obj.Id;
            return executeNonQuery(sql) > 0;
        }
        /*
        * @todo delete order detail from database
        * */
        public bool deleteOrderDetail(Order obj)
        {
            string sql = "DELETE FROM ap_order_detail WHERE order_id=" + obj.Id;
            return executeNonQuery(sql) > 0;
        }
        /*
         * 
         * */
        public static List<OrderDetail> Ap_order_detail(Order obj)
        {
            string sql = "SELECT o.*, p.name AS product_name";
            sql += " FROM ap_order_detail o INNER JOIN ap_product p";
            sql += " ON o.product_id = p.id";
            sql += " WHERE o.order_id = " + obj.Id;
            DataTable dt = read(sql);
            List<OrderDetail> list = new List<OrderDetail>();
            foreach (DataRow dr in dt.Rows)
            {
                list.Add(new OrderDetail(dr));
            }
            return list;
        }
    }
}
