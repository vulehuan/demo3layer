﻿using System;
using System.Collections.Generic;
using System.Text;
using demo3Layer.dto;
using demo3Layer.libs;
using System.Data;

namespace demo3Layer.dal
{
    class ProductDal:APCoreDbConnect
    {
        public ProductDal()
        {
        }
        /*
         * @todo get object by product code
         * */
        public Product getObjectByProductCode(Product obj)
        {
            List<Product> result = new List<Product>();
            string sql = "SELECT * FROM ap_product WHERE code = '" + APCoreFilter.clean(obj.Code) + "'";
            if (obj.Id > 0)
            {
                sql += " AND id!=" + obj.Id;
            }
            DataTable dt = read(sql);
            if (dt.Rows.Count == 0)
            {
                return null;
            }

            return new Product(dt.Rows[0]);
        }
        /*
         * @todo get product list from database
         * */
        public static List<Product> getList()
        {
            List<Product> result = new List<Product>();
            DataTable dt = read("ap_product");
            foreach (DataRow dr in dt.Rows)
            {
                result.Add(new Product(dr));
            }

            return result;
        }
        public static List<Product> getList(string keyword)
        {
            List<Product> result = new List<Product>();
            keyword = APCoreFilter.clean(keyword);
            DataTable dt = read("SELECT * FROM ap_product WHERE name like '%" + keyword + "%' OR code like '%" + keyword + "%'");
            foreach (DataRow dr in dt.Rows)
            {
                result.Add(new Product(dr));
            }

            return result;
        }
        /*
         * @todo insert new object to database
         * */
        public bool insert(Product obj)
        {
            DataTable dt = readStructure("ap_product");
            DataRow dr = dt.NewRow();
            dr["name"] = obj.Name;
            dr["code"] = obj.Code;
            dr["price"] = obj.Price;
            dt.Rows.Add(dr);

            return write(dr) > 0;
        }
        /*
         * @todo update object information to database
         * */
        public bool update(Product obj)
        {
            //DataTable dt = read("ap_product");
            DataTable dt = read("SELECT * FROM ap_product WHERE id=" + obj.Id);
            DataRow dr = dt.Rows.Find(obj.Id);
            dr["name"] = obj.Name;
            dr["code"] = obj.Code;
            dr["price"] = obj.Price;

            return write(dr) > 0;
        }
        /*
         * @todo delete object from database
         * */
        public bool delete(Product obj)
        {
            //DataTable dt = read("ap_product");
            //DataRow dr = dt.Rows.Find(obj.Id);
            //dr.Delete();
            //return write(dr) > 0;

            string sql = "DELETE FROM ap_product WHERE id=" + obj.Id;
            return executeNonQuery(sql) > 0;
        }
    }
}
