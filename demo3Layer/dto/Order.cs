﻿using System;
using System.Data;
using System.Collections.Generic;

namespace demo3Layer.dto
{
	public class Order
	{
		private int id;
		
		public int Id {
			get { return id; }
			set { id = value; }
		}
		private DateTime date;
		
		public DateTime Date {
			get { return date; }
			set { date = value; }
		}
		private string customer;
		
		public string Customer {
			get { return customer; }
			set { customer = value; }
		}
		private decimal total;
		
		public decimal Total {
			get { return total; }
			set { total = value; }
		}
        private List<OrderDetail> ap_order_detail;

        public List<OrderDetail> Ap_order_detail
        {
            get { return ap_order_detail; }
            set { ap_order_detail = value; }
        }
 
		public Order()
		{
		}
        public Order(DataRow dr)
        {
            this.Id = int.Parse(dr["id"].ToString());
            this.Customer = dr["customer"].ToString();
            this.Date = (DateTime)dr["date"];
            this.Total = (decimal)dr["total"];
        }
	}
    public class OrderDetail
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private int order_id;

        public int Order_id
        {
            get { return order_id; }
            set { order_id = value; }
        }
        private int product_id;

        public int Product_id
        {
            get { return product_id; }
            set { product_id = value; }
        }
        private int amount;

        public int Amount
        {
            get { return amount; }
            set { amount = value; }
        }
        private decimal price;

        public decimal Price
        {
            get { return price; }
            set { price = value; }
        }
        //addition field
        private string product_name;

        public string Product_name
        {
            get { return product_name; }
            set { product_name = value; }
        }

        //
        public OrderDetail()
        {
        }
        public OrderDetail(DataRow dr)
        {
            this.id = (int)dr["id"];
            this.order_id = (int)dr["order_id"];
            this.product_id = (int)dr["product_id"];
            this.amount = (int)dr["amount"];
            this.price = (decimal)dr["price"];
            this.product_name = (string)dr["product_name"];
        }
    }
}
