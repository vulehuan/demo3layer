﻿using System;
using System.Data;

namespace demo3Layer.dto
{
	public class Product
	{
		private int id;
		
		public int Id {
			get { return id; }
			set { id = value; }
		}
		private string name;
		
		public string Name {
			get { return name; }
			set { name = value; }
		}
		private string code;
		
		public string Code {
			get { return code; }
			set { code = value; }
		}
		private decimal price;
		
		public decimal Price {
			get { return price; }
			set { price = value; }
		}
		
		public Product()
		{
		}
        public Product(DataRow dr)
        {
            this.id = int.Parse(dr["id"].ToString());
            this.name = dr["name"].ToString();
            this.code = dr["code"].ToString();
            this.price = decimal.Parse(dr["price"].ToString());
        }
	}
}
